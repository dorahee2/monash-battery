#User Guide

#### Install dependencies
```pip install monash-battery minizinc pandas numpy pandas-bokeh pathlib```

* Use the default versions will do. 

#### Install MiniZinc

1. download MiniZinc 2.5.5 IDE from ```https://www.minizinc.org/``` and install.

2. follow the configuration instructions in the MiniZinc handbook ```https://www.minizinc.org/doc-2.5.5/en/installation.html```

   * for MAC user,  run the following command in the terminal 
    
    ```$ export PATH=/Applications/MiniZincIDE.app/Contents/Resources:$PATH```

#### Run the program

1. create  ```batteries.json```, ```charges.json```, ```loads.json``` following the formats of the sample files in the ```data``` folder. 

2. provide the locations of ```batteries.json```, ```charges.json```, ```loads.json```

3. call the algorithm as follows: 
```
batteries_json, charges_json, previous_solutions_json = \
                opt.operation(batteries_json=None, batteries_json_file=batteries_json_file,
                              charges_json=None, charges_json_file=charges_json_file,
                              loads_json=None, loads_json_file=loads_json_file,
                              previous_solutions_json=None,
                              previous_solutions_json_file=previous_solutions_json_file,
                              solver=solver)
```
                            
* the results are written to the ```batteries.json```, ```charges.json```, ```loads.json``` and ```solutions.json```, and outputted as JSON strings.

#### Rerun the program in real time

1. fetch updated battery, charges and loads data, and organise them in json files or json strings.

2. pass the updated data to the program and run it again.    