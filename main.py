from monash_battery import operation as opt

# specify the locations of the battery, charges, loads and previous_solutions json file
# you can do this in your JAVA system
folder = "data"
batteries_json_file = f"{folder}/batteries.json"
charges_json_file = f"{folder}/charges.json"
loads_json_file = f"{folder}/loads.json"
previous_solutions_json_file = f"{folder}/solutions.json"
solver = "mip"

# call this function is all you need for getting the battery schedule
# the result will be written back to the json files specified above and output as json strings
batteries_json, charges_json, previous_solutions_json = \
                opt.operation(batteries_json=None, batteries_json_file=batteries_json_file,
                              charges_json=None, charges_json_file=charges_json_file,
                              loads_json=None, loads_json_file=loads_json_file,
                              previous_solutions_json=None,
                              previous_solutions_json_file=previous_solutions_json_file,
                              solver=solver)
print("")
